package eu.cabrera.ideutils;

import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * Created by antoine on 23/07/2014.
 */
public class IdeUtil {

    public static int dpToPx(int dp,WindowManager window2) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        window2.getDefaultDisplay().getMetrics(displayMetrics);

        float logicalDensity = displayMetrics.density;
        return (int) Math.ceil(dp * logicalDensity);

    }
    public static int pxToDp(int px,WindowManager window2) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        window2.getDefaultDisplay().getMetrics(displayMetrics);

        float logicalDensity = displayMetrics.density;
        return (int) Math.ceil(px / logicalDensity);
    }
}