package eu.cabrera.ideutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import eu.cabrera.ingresstools.targetap.R;

/**
 *
 * Created by antoine on 04/07/2014.
 */
public class MenuView extends RelativeLayout {

    Bitmap.Config conf ;
    Bitmap bmp ;
    Paint paint ;

    public MenuView(Context context) {
        super(context);
        this.setWillNotDraw(false);

        conf = Bitmap.Config.ARGB_8888; // see other conf types

        paint = new Paint();
    }

    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setWillNotDraw(false);

        conf = Bitmap.Config.ARGB_8888; // see other conf types

        paint = new Paint();
    }

    public MenuView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
        this.setWillNotDraw(false);

       conf = Bitmap.Config.ARGB_8888; // see other conf types

        paint = new Paint();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (getHeight()>0 && getWidth()>0) {

            bmp = Bitmap.createBitmap(getWidth(), getHeight(), conf); // this creates a MUTABLE bitmap

            paint.setColor(getResources().getColor(R.color.winborders));
            paint.setStyle(Paint.Style.STROKE);

            canvas.drawRect(0, 0, getWidth() - 1, getHeight() - 1, paint);

            canvas.drawBitmap(bmp, 0, 0, null);
        }

    }
}