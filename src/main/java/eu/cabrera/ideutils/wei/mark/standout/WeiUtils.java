package eu.cabrera.ideutils.wei.mark.standout;

public class WeiUtils {
	public static boolean isSet(int flags, int flag) {
		return (flags & flag) == flag;
	}
}
