package eu.cabrera.ingresstools.targetap;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

import eu.cabrera.ideutils.TypefaceSpan;

/**
 *
 * Activity A Propos : infos et aide sur l'application
 */
public class AboutActivity extends ActionBarActivity implements View.OnClickListener{

    private Button btn_paypal;

    @Override
    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        EasyTracker.getInstance(this).activityStart(this);  // GAnalytics.
    }

    @Override
    public void onStop() {
        super.onStop();
        // The rest of your onStop() code.
        EasyTracker.getInstance(this).activityStop(this);  // GAnalytics.
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        String laVersion = "" ;
        TextView version = (TextView) findViewById(R.id.versionNumber);
        try {
            laVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        }
        catch (Exception e)
        {
            //Log.d("EXCEPT", e.getMessage());
        }

        version.setText(laVersion);

        btn_paypal = (Button) findViewById(R.id.buttonpaypal);

        btn_paypal.setOnClickListener(this) ;

    setFonts();
    }

    private void setFonts(){
        // Begin SetFonts
        // Font path
        String fontPathR = "Coda-Regular.ttf";

        // Loading Font Face
        Typeface tfCodaR = Typeface.createFromAsset(getAssets(), fontPathR);

        // Comment
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            SpannableString s = new SpannableString(getResources().getString(R.string.title_about));
            s.setSpan(new TypefaceSpan(this, fontPathR), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ActionBar actionBar = getActionBar();
            if (actionBar != null) actionBar.setTitle(s);
        }

        // Applying font
        ((TextView) findViewById(R.id.appTitle)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.versionNumber)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.textView)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.textView2)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.textView4)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.textView5)).setTypeface(tfCodaR);

        ((Button) findViewById(R.id.buttonpaypal)).setTypeface(tfCodaR);

        // End SetFonts
    }

    @Override
    public void onClick(View v){
        if (v == btn_paypal){
            Intent browserIntent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.link_paypal)));
            startActivity(browserIntent);
        }
    }

}