package eu.cabrera.ingresstools.targetap;

import android.support.v7.app.ActionBarActivity;

/**
 * Abstracteur pour donner accès aux méthodes de l'activité à la CalcEngine
 * Created by antoine on 01/05/2014.
 */
public abstract class ActivityAbstractor extends ActionBarActivity {

    public abstract void showResults() ;
    public abstract void calcFinished(boolean interrupted, boolean solOk) ;
    public abstract void updateResults(int totalActions, int[] indexes) ;

    public abstract void updateProgressBar(int progress) ;
    public abstract void updateProgressMsg(String msg) ;
}
