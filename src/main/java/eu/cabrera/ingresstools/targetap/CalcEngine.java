package eu.cabrera.ingresstools.targetap;

import eu.cabrera.ingresstools.targetap.datastructures.ActionAp;
import eu.cabrera.mathutils.MathUtil;

/**
 * La magie s'opère ici
 *
 * Created by antoine on 01/05/2014.
 */
final class CalcEngine {
    private final int diff ;
    private int nbLoops = 0 ;
    private Task task ;

    private final int[] maxLoop ;
    private final ActionAp[] actionNames ;
    private boolean solOk ;

    private int minimum ;
    private int nbSolutions ;

    private long nbTotalCombinations ;
    private long currentTotalCombinations ;

    private final ActivityAbstractor activity ;


    CalcEngine(int initialAp, int targetAp, ActionAp[] actionNames, ResultActivity activity){
        int[] neededAction ;

        this.diff = targetAp - initialAp ;

        this.actionNames = actionNames ;

        this.activity = activity ;

        this.maxLoop = new int[actionNames.length];

        this.solOk = false ;
        // this.oldTotal = 0 ;
        this.nbSolutions = 0 ;

        nbTotalCombinations = 1 ;
        currentTotalCombinations = 0 ;

        neededAction = new int[actionNames.length];
        for (int i = 0; i<actionNames.length; i++){
            neededAction[i] = 0 ;
        }

        for (int i = 0; i<actionNames.length; i++){
            maxLoop[i] = diff/actionNames[i].ap;
            if (maxLoop[i]>0) nbLoops++;
            // Log.d("CAL","1 maxLoop[" + i + "]=" + maxLoop[i]);
        }

        //Log.d("CAL","nbLoops =" + nbLoops);

        int ppcm ;
        for (int i = 0; i<nbLoops; i++){
            for (int j=i+1; j<nbLoops; j++){
                ppcm = MathUtil.getPpcm(actionNames[i].ap, actionNames[j].ap) ;
                maxLoop[i] = Math.min(maxLoop[i],(ppcm/actionNames[i].ap)-1);
            }
        }

        for (int i = 0; i<nbLoops; i++){
            if (maxLoop[i] !=0) nbTotalCombinations *= (maxLoop[i]+1) ;
            //Log.d("calc","maxLoop[" + i + "]=" + maxLoop[i]) ;
        }

        //Log.d("calc","nbTotalCombinations=" + nbTotalCombinations ) ;

    }

    protected void processCalculation(){
        this.task = new Task() ;
        new Thread(task).start();
    }

    protected void stopCalcultation(){
        this.task.running = false ;
    }

    class Task implements Runnable {

        private boolean running ;
        private final int[] indexes  = new int[nbLoops];
        private long startTime ;
        private boolean shownResults = false ;

        @Override
        public void run() {
            running = true ;
            nbSolutions = 0 ;
            startTime = System.nanoTime() ;

            activity.updateProgressMsg(activity.getResources().getString(R.string.calculating) + " " +
                    activity.getResources().getString(R.string.touch_screen_to_interrupt));
            activity.updateProgressBar(0) ;

            boolean resultSearch = searchLoop(0) ;
            //Log.d("results", "Elapsed time=" + elapsedTime);

            activity.calcFinished(!resultSearch, solOk);
            //Log.d("calc","nbCombinations=" + currentTotalCombinations) ;
        }

        private boolean searchLoop(final int currentIndex){

            // Faire le cas ou c'est un multiple dès le début ou inverser l'ordre des boucles
            for (indexes[currentIndex]=0; indexes[currentIndex] <= maxLoop[currentIndex]; indexes[currentIndex]++){
                //Log.d("calc", "indexes[" + currentIndex + "]=" + indexes[currentIndex]) ;

                if (currentIndex == 0) activity.updateProgressBar((int)((100*currentTotalCombinations)/nbTotalCombinations));

                if ((currentIndex+1) < nbLoops){
                    if (!running) return false ; // Test si on stoppe le calcul en cours
                    searchLoop(currentIndex+1);
                }

                if (currentIndex+1 == nbLoops) { // Dernier niveau, on calcule
                    currentTotalCombinations++ ;
                    int somme = 0 ;
                    for (int i=0; i<=currentIndex; i++) {
                        somme += indexes[i]*actionNames[i].ap ;
                        if (somme > diff) break ;
                    }

                    if (somme > diff) break ;


                    if (somme == diff){

                        int totalActions = 0 ;
                        for (int i=0; i<nbLoops; i++) totalActions += indexes[i] ;

                        if (!solOk || (solOk && minimum>totalActions)) {
                            minimum = totalActions ;
                            solOk = true ;
                        }

                        String msg = activity.getResources().getString(R.string.calculating) + "\n" + nbSolutions + " ";
                        msg += (nbSolutions == 1) ? activity.getResources().getString(R.string.nb_combinations_one) : activity.getResources().getString(R.string.nb_combinations_plural);
                        msg += "\n" + minimum + " ";
                        msg += (minimum == 1) ? activity.getResources().getString(R.string.nb_actions_required_one) : activity.getResources().getString(R.string.nb_actions_required_plural);
                        msg += " " + activity.getResources().getString(R.string.touch_screen_to_interrupt);

                        if (nbSolutions >1) msg += "\n" + activity.getResources().getString(R.string.msg_swipe);


                        activity.updateProgressMsg(msg);

                        if (totalActions<=minimum+Constants.ACTION_NUMBER_MAXGAP){ // On conserve des solutions avec N actions de plus, il faudra nettoyer la liste finale aussi
                            nbSolutions++;

                            activity.updateResults(totalActions, indexes);
                        }
                        if (!shownResults && (((System.nanoTime() - startTime)/1000000000)>Constants.MIN_CALCTIME_BEFORE_RESULTS)){ // Si on a des résultats avant N secondes, on affiche et on ne met pas à jour après
                            activity.showResults();
                            shownResults = true ;
                        }

                    }

                }

            }
            return true ; // Non interrompu
        }


    }

}
