package eu.cabrera.ingresstools.targetap;

/**
 *
 * Constantes
 * Created by antoine on 01/05/2014.
 */
final class Constants {
    public final static String EXTRA_MESSAGE = "eu.cabrera.ingresstools.targetap.MESSAGE";
    public final static int MAX_DIFF = 1000000 ;

    // Splash screen timer
    public final static int SPLASH_TIME_OUT = 1000 ;

    public final static int MIN_CALCTIME_BEFORE_RESULTS = 1 ; // In secs

    public final static int ACTION_NUMBER_MAXGAP = 10 ;

    public final static int TYPE_BUILDER = 0 ;
    public final static int TYPE_BUILDER_FIELDER = 1 ;
    public final static int TYPE_FULL = 2 ;
    public final static int TYPE_PERSO = 3 ;

    public final static String PREFS_NAME = "TargetAPPrefs_11" ;

    public final static int AP_MAXGAP_TO_SUGGESTED = 1000000 ;
    public final static int NB_MAX_SUGGESTIONS = 10 ;
    public final static int MIN_SUGGEST_SIZE = 5 ;

    public final static int WEI_TARGETAP_AP_WINDOW_ID = 0 ;
    public final static int WEI_TARGETAP_RESULTS_WINDOW_ID = 1 ;

    public final static int WEI_DATAMSG_REQCODE_SHOW = 0 ;
    public final static int WEI_DATAMSG_REQCODE_HIDE = 1 ;

    public final static int SOL_POPUP_MAX_WIDTH = 300 ; // Max Popup Size in dp
    public final static int SOL_POPUP_MAX_HEIGHT = 300 ; // Max Popup Size in dp

    public final static int AP_POPUP_MAX_WIDTH = 300 ; // Max Popup Size in dp
    public final static int AP_POPUP_MAX_HEIGHT = 65 ; // Max Popup Size in dp

    private Constants() {}

}
