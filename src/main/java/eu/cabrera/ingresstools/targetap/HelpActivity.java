package eu.cabrera.ingresstools.targetap;

import android.app.ActionBar;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;

import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

import eu.cabrera.ideutils.TypefaceSpan;


public class HelpActivity extends ActionBarActivity {

    @Override
    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        EasyTracker.getInstance(this).activityStart(this);  // GAnalytics.
    }

    @Override
    public void onStop() {
        super.onStop();
        // The rest of your onStop() code.
        EasyTracker.getInstance(this).activityStop(this);  // GAnalytics.
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        setFonts();
    }

    private void setFonts(){
        // Begin SetFonts
        // Font path
        String fontPathR = "Coda-Regular.ttf";

        // Loading Font Face
        Typeface tfCodaR = Typeface.createFromAsset(getAssets(), fontPathR);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            SpannableString s = new SpannableString(getResources().getString(R.string.title_help));
            s.setSpan(new TypefaceSpan(this, fontPathR), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ActionBar actionBar = getActionBar();
            if (actionBar != null) actionBar.setTitle(s);
        }

        // Applying font
        ((TextView) findViewById(R.id.help_options)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.help_results)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.help_title_options)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.help_title_results)).setTypeface(tfCodaR);

        // End SetFonts
    }

}
