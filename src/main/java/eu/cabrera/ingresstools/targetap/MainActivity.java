package eu.cabrera.ingresstools.targetap;

import android.app.ActionBar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import java.util.Iterator;

import com.google.analytics.tracking.android.EasyTracker;

//import eu.cabrera.app;
import eu.cabrera.ideutils.TypefaceSpan;
import eu.cabrera.ingresstools.targetap.datastructures.ActionAp;
import eu.cabrera.ingresstools.targetap.datastructures.DataInWrapper;
import eu.cabrera.ingresstools.targetap.datastructures.OptionListViewAdapter;
import eu.cabrera.ingresstools.targetap.datastructures.PatternNumber;
import eu.cabrera.ideutils.wei.mark.standout.WeiStandOutWindow;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private ActionAp[] initialActionNames ;

    private int diff;
    private int nbSel = 0;

    private int initialAp ;
    private int targetAp ;

    private int currentSpinner = 0 ;


    private EditText editTextActual ;
    private EditText editTextTarget ;

    private Spinner preselectionsSpinner ;

    private static final String TAG = "main" ;

    private final boolean [][] preSelect = {
            {true, true, false, true, true, false, false, true, false, true, false, false, true, false, false}, // Builder no links
            {true, true, false, true, true, false, true, true, false, true, false, true, true, false, false}, // Builder & Linker
            {true, true, true, true, true, true, true, true, true, true, true, true, true, true, true} // Full
    };

    private final ArrayList<ActionAp> list = new ArrayList<ActionAp>();

    private Button buttonSuggest ;
    private Button buttonCalculate ;
    private Button buttonGetAp;

    @Override
    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        EasyTracker.getInstance(this).activityStart(this);  // GAnalytics.
   }

    @Override
    public void onStop() {
        super.onStop();
        // The rest of your onStop() code.
        EasyTracker.getInstance(this).activityStop(this);  // GAnalytics.

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("selectionSpinner", currentSpinner);
        if (currentSpinner == Constants.TYPE_PERSO){
            boolean tempo[] = new boolean[initialActionNames.length];
            for (int i=0; i< initialActionNames.length; i++){
                tempo[i] = initialActionNames[i].getStatus();
            }
            editor.putString("selectionOptions", optionsToString(tempo));
        }

        editor.putString("initialAP", "" + editTextActual.getText());
        editor.putString("targetAP", "" + editTextTarget.getText());

        // Commit the edits!
        editor.commit();

        // WeiStandOutWindow.closeAll(getApplicationContext(), PopApWindow.class);

    }

    private void setFonts(){
        // Begin SetFonts
        // Font path
        String fontPathR = "Coda-Regular.ttf";

        // Loading Font Face
        Typeface tfCodaR = Typeface.createFromAsset(getAssets(), fontPathR);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            SpannableString s = new SpannableString(getResources().getString(R.string.app_name));
            s.setSpan(new TypefaceSpan(this, fontPathR), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ActionBar actionBar = getActionBar();
            if (actionBar != null) actionBar.setTitle(s);
        }

        // Applying font
        ((TextView) findViewById(R.id.labelEnterAp)).setTypeface(tfCodaR);
        ((EditText) findViewById(R.id.actualAp)).setTypeface(tfCodaR);
        ((EditText) findViewById(R.id.targetAp)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.labelSelectOptions)).setTypeface(tfCodaR);
        ((Button) findViewById(R.id.buttonCalculate)).setTypeface(tfCodaR);
        ((Button) findViewById(R.id.buttonSuggest)).setTypeface(tfCodaR);

        ((Button) findViewById(R.id.buttongetap)).setTypeface(tfCodaR);

        // End SetFonts
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Log.d(TAG,"onCreate") ;

        final ListView lViewA;

        buttonGetAp = (Button) findViewById(R.id.buttongetap) ;
        buttonGetAp.setOnClickListener(this);

        buttonCalculate = (Button) findViewById(R.id.buttonCalculate);
        buttonCalculate.setOnClickListener(this);

        buttonSuggest = (Button)findViewById(R.id.buttonSuggest);
        buttonSuggest.setOnClickListener(this) ;

        // Restore preferences
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
        int savedSelections = settings.getInt("selectionSpinner", Constants.TYPE_BUILDER);
        String savedInitialAP = settings.getString("initialAP", "");
        String savedTargetAP = settings.getString("targetAP", "");

        editTextActual =  (EditText) findViewById(R.id.actualAp);
        editTextTarget =  (EditText) findViewById(R.id.targetAp);

        preselectionsSpinner = (Spinner) findViewById(R.id.preselections_spinner);

        setFonts() ;

        initialActionNames = new ActionAp[] {
                new ActionAp(getResources().getString(R.string.recharge_portal),          10, true),
                new ActionAp(getResources().getString(R.string.upgrade_resonator),        65, true),
                new ActionAp(getResources().getString(R.string.destroy_resonator),        75, false),
                new ActionAp(getResources().getString(R.string.hack_enemy_portal),       100, true),
                new ActionAp(getResources().getString(R.string.place_resonator_mod),     125, true),
                new ActionAp(getResources().getString(R.string.destroy_link),            262, false), // 187 + 75
                new ActionAp(getResources().getString(R.string.link_portals),            313, true),
                new ActionAp(getResources().getString(R.string.complete_portal),         375, true), // 250+125
                new ActionAp(getResources().getString(R.string.destroy_portal),          600, false),
                new ActionAp(getResources().getString(R.string.capture_portal),          625, true), // 500 + 125
                new ActionAp(getResources().getString(R.string.destroy_field),           1199, false), // 750+2x187+75
                new ActionAp(getResources().getString(R.string.establish_field),        1563, true), // 1250+313
                new ActionAp(getResources().getString(R.string.capture_deploy),         1750, false), // 500+8x125+250+600
                new ActionAp(getResources().getString(R.string.destroy_deploy_portal),  2350, false), // 500+8x125+250
                new ActionAp(getResources().getString(R.string.establish_double_field), 2813, false), // 1250+1250+313
        };

        lViewA = (ListView) findViewById(R.id.ListViewA);

        final OptionListViewAdapter adapter = new OptionListViewAdapter(this, R.layout.activity_main, R.layout.mylist, list);

        lViewA.setAdapter(adapter) ;

        //adapter.notifyDataSetChanged();

        //lViewA.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        ArrayList<String> preselectionsList = new ArrayList<String>();

        preselectionsList.add(getResources().getString(R.string.radio_builder));
        preselectionsList.add(getResources().getString(R.string.radio_builder_linker));
        preselectionsList.add(getResources().getString(R.string.radio_full));
        preselectionsList.add(getResources().getString(R.string.radio_personal));

        ArrayAdapter dataAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, preselectionsList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        preselectionsSpinner.setAdapter(dataAdapter);

        preselectionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Log.d("main", "Spinner " + i) ;
                currentSpinner = i;
                switch (i) {
                    case Constants.TYPE_BUILDER:
                        initialiseSelections(Constants.TYPE_BUILDER);
                        adapter.notifyDataSetChanged();
                        break;
                    case Constants.TYPE_BUILDER_FIELDER:
                        initialiseSelections(Constants.TYPE_BUILDER_FIELDER);
                        adapter.notifyDataSetChanged();
                        break;
                    case Constants.TYPE_FULL:
                        initialiseSelections(Constants.TYPE_FULL);
                        adapter.notifyDataSetChanged();
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lViewA.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OptionListViewAdapter.ViewHolder holder = (OptionListViewAdapter.ViewHolder) view.getTag();
                CheckBox cb = holder.checkBox;
                TextView tx = holder.text1;

                initialActionNames[position].setStatus(!initialActionNames[position].getStatus());

                preselectionsSpinner.setSelection(Constants.TYPE_PERSO);// Pour toute action sur la liste TODO: vérifier si ça correspond à un modèle et activer le bon bouton

                if (initialActionNames[position].getStatus()) {

                    nbSel++;
                    cb.setChecked(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        tx.setActivated(true);
                    } else {
                        tx.setSelected(true);
                    }
                } else {

                    nbSel--;
                    cb.setChecked(false);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        tx.setActivated(false);
                    } else {
                        tx.setSelected(false);
                    }
                }
                updateButtonStatus();
            }

        });

        editTextActual.setOnKeyListener(new AdapterView.OnKeyListener(){
            @Override
            public boolean onKey(View view, int k, KeyEvent ke){

                updateButtonStatus();
                return false ;
            }
        });

        editTextTarget.setOnKeyListener(new AdapterView.OnKeyListener(){
            @Override
            public boolean onKey(View view, int k, KeyEvent ke){

                updateButtonStatus();
                return false ;
            }
        });

        preselectionsSpinner.setSelection(savedSelections);

        if (savedSelections == Constants.TYPE_PERSO){
            String selectionOptionsString = settings.getString("selectionOptions",optionsToString( preSelect[Constants.TYPE_BUILDER]));
            initialiseSelections(optionsToBooleanArray(selectionOptionsString));
        }
        else {
            initialiseSelections(savedSelections); // Défault Builder
        }

        editTextActual.setText(savedInitialAP);
        editTextTarget.setText(savedTargetAP);

        Intent intent = getIntent();

        processApIntent(intent);
    }

    @Override
    public void onClick(View v){
        if (v == buttonSuggest) {
            try{
                suggestTargetAp(Integer.parseInt(editTextActual.getText().toString()));
            }
            catch (Exception e)
            {
                if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.actual_ap_missing), Toast.LENGTH_LONG)
                        .show();
            }
        }
        if (v == buttonCalculate) {
            processForm();
        }
        if (v == buttonGetAp){
            popGetAp();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        processApIntent(intent);
    }

    private void processApIntent(Intent intent) {
        if (intent.hasExtra("AP")) {
            // TODO Auto-generated method stub
            final int datapassed = intent.getIntExtra("AP", 0);

            Toast.makeText(MainActivity.this, getResources().getString(R.string.popapsent), Toast.LENGTH_LONG).show();

            editTextActual.setText("" + String.valueOf(datapassed));

            hide();
        }

        if (intent.hasExtra("CLOSEMEAP")) {
            //Log.d(TAG,"Receive Intent CLOSEMEAP");

            hide(); // WeiStandOutWindow.close(getApplicationContext(), MemoMapWindow.class, Constants.WEI_MEMOMAP_WINDOW_ID);
        }
    }

    private void show(){
        //Log.d(TAG,"show pop ap");
        Bundle data = new Bundle();
        data.putInt("id",Constants.WEI_TARGETAP_AP_WINDOW_ID);
        WeiStandOutWindow.sendData(getApplicationContext(), PopApWindow.class, WeiStandOutWindow.DISREGARD_ID,
                Constants.WEI_DATAMSG_REQCODE_SHOW, data, null, WeiStandOutWindow.DISREGARD_ID);
    }

    private void hide(){
        //Log.d(TAG,"hide pop ap");
        Bundle data = new Bundle();
        data.putInt("id",Constants.WEI_TARGETAP_AP_WINDOW_ID);
        WeiStandOutWindow.sendData(getApplicationContext(), PopApWindow.class, WeiStandOutWindow.DISREGARD_ID,
                Constants.WEI_DATAMSG_REQCODE_HIDE, data, null, WeiStandOutWindow.DISREGARD_ID);
    }

    private void popGetAp(){

        //Log.d(TAG,"popGetAp");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                show();

            }
        });

    }

    private boolean updateButtonStatus(){

        boolean actualMissing = false ;
        boolean targetMissing = false ;

        initialAp = 0 ;
        try {
            if (editTextActual.getText() != null) {
                initialAp = Integer.parseInt(editTextActual.getText().toString());
            }
        }
        catch (Exception e){
            actualMissing = true ;
        }
        targetAp = 0 ;
        try {
            if (editTextTarget.getText() != null) {
                targetAp = Integer.parseInt(editTextTarget.getText().toString());
            }
        }
        catch (Exception e){
            targetMissing = true ;
        }

        return ! (nbSel == 0 || actualMissing || targetMissing) ;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity, menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up btn_paypal, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_about:
                openAbout();
                return true;
            case R.id.action_help:
                openHelp();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    private void openHelp() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }


    private boolean validateEntries(){

        initialAp = 0 ;
        try {
            if (editTextActual.getText() != null) {
                initialAp = Integer.parseInt(editTextActual.getText().toString());
            }
        }
        catch (Exception e){
            if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.actual_ap_missing), Toast.LENGTH_LONG)
                    .show();
        }
        targetAp = 0 ;
        try {
            if (editTextTarget.getText() != null) {
                targetAp = Integer.parseInt(editTextTarget.getText().toString());
            }
        }
        catch (Exception e){
            if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.target_ap_missing), Toast.LENGTH_LONG)
                    .show();
        }

        diff = targetAp - initialAp ;

        if (diff == 0){
            if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.ap_diff_null), Toast.LENGTH_LONG)
                    .show();
            return false;
        }
        if (diff > Constants.MAX_DIFF){
            if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.ap_diff_toohigh) + " " + Constants.MAX_DIFF, Toast.LENGTH_LONG)
                    .show();
            return false;
        }
        if (diff < 0){
            if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.valeur_depart) + ">" + getResources().getString(R.string.valeur_arrivee), Toast.LENGTH_LONG)
                    .show();
            return false;
        }

        return true ;
    }

    private void processForm(){
        ActionAp[] actionNames ;

        if (!updateButtonStatus()) return ;
        if (!validateEntries()) return ;

        // populate actionNames with selections
        int cpt=0;
        actionNames = new ActionAp[nbSel];

        for (ActionAp initialActionName : initialActionNames){
            if (initialActionName.getStatus()) actionNames[cpt++] = new ActionAp(initialActionName.getActionName(),initialActionName.getAp(),false);
        }

        // Contrôler les écarts minimums possibles avec la liste des sélections
        // Si par exemple on a diff = 20 et qu'on saisie à partir de 65, c'est dead

        int foundMultiple = 0 ;
        for (ActionAp actionName : actionNames){
            foundMultiple += diff/(actionName.getAp()) ;
        }
        if (foundMultiple == 0){
            if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.diff_too_low), Toast.LENGTH_LONG)
                    .show();
            return ;
        }

        // Ajouter les contrôles basiques pour réussir le dernier chiffre avec les combinaisons sélectionnées

        int lastDigit = diff%10 ;

        switch (lastDigit){
            case 1 : // Valeurs ne pouvant être obtenues qu'avec une combinaison d'impairs différents de 5
            case 2 :
            case 3 :
            case 4 :
            case 6 :
            case 7 :
            case 8 :
            case 9 :
                boolean foundEven = false ;
                for (ActionAp actionName : actionNames){
                    if ((actionName.getAp()+1)%2 == 0 && !((actionName.getAp())%5 == 0)) foundEven = true ;
                }
                if (!foundEven){
                    if (getApplicationContext() != null) Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.need_1379), Toast.LENGTH_LONG)
                            .show();
                    return ;
                }
                break ;
        }

        DataInWrapper dataInWrapper = new DataInWrapper(initialAp, targetAp, actionNames);

        Intent intentResult;
        intentResult = new Intent(this, ResultActivity.class);
        intentResult.putExtra(Constants.EXTRA_MESSAGE, dataInWrapper);
        startActivity(intentResult);
    }

    private void initialiseSelections(int sel){
        initialiseSelections(preSelect[sel]);
    }

    private void initialiseSelections (boolean[] sel){
        nbSel = 0 ;
        for(int i=0; i<initialActionNames.length; i++){
            initialActionNames[i].setStatus(sel[i]);
        }
        list.clear();
        for (int i=0; i<initialActionNames.length; i++) {
            if (initialActionNames[i].getStatus()) nbSel++;
            list.add(i, initialActionNames[i]);
        }
    }

    private String optionsToString (boolean[] selections){
        String result = "" ;

        for( boolean selection : selections){
            result += (selection)?"1":"0";
        }
        return result ;
    }

    private boolean[] optionsToBooleanArray(String selectionString){
        boolean [] result = new boolean[selectionString.length()];

        for(int i=0; i<selectionString.length();i++){
            result[i] = (selectionString.charAt(i) != '0');
        }
        return result ;
    }

    private void suggestTargetAp(int actualAp) {

        SuggestionEngine sE = new SuggestionEngine();

        final ArrayList<PatternNumber> patterns = sE.getPatternNumbers(actualAp);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.title_list_suggestion));
        final String[] array = new String[Math.min(patterns.size(), Constants.NB_MAX_SUGGESTIONS)];

        Iterator it = patterns.iterator();
        int index = 0 ;
        PatternNumber myPatternNumber ;

        while (it.hasNext() && (index < Constants.NB_MAX_SUGGESTIONS)) {
            myPatternNumber = (PatternNumber)(it.next()) ;
            array[index++] = (myPatternNumber).getNumber() + " (" + getResources().getString((myPatternNumber).getNameResId()) + ")";
        }

        builder.setItems(array, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                //Log.d("main","item=" + item);
                editTextTarget.setText(patterns.get(item).getNumber());

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
 }



}
