package eu.cabrera.ingresstools.targetap;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import eu.cabrera.ingresstools.targetap.datastructures.ActionResult;
import eu.cabrera.ingresstools.targetap.datastructures.ResultListViewAdapter;


public class MyFragment extends Fragment{

    private int mCurrentPage;
    private int currentActions;
    private int nbSol;
    private int[] currentResults;

    private String[] actionNamesString ;
    private int[] actionNamesAP ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        /** Getting integer data of the key current_page from the bundle */
        mCurrentPage = data.getInt("current_page", 0);
        nbSol = data.getInt("nb_sol",0);
        currentActions = data.getInt("current_actions",0);
        currentResults = data.getIntArray("current_results");

        actionNamesString = data.getStringArray("action_names");
        actionNamesAP = data.getIntArray("action_ap");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_results, container,false);

        TextView topmsg = (TextView ) v.findViewById(R.id.topmsg2);
        String msg = getResources().getString(R.string.msg_combination) + " " + (mCurrentPage+1) + " " + getResources().getString(R.string.msg_of) + " " + nbSol + " : " + currentActions + " ";
        msg += (currentActions == 1) ? getResources().getString(R.string.nb_actions_required_one) : getResources().getString(R.string.nb_actions_required_plural);

        topmsg.setText(msg);

        ActionResult[] actionResults;

        int[] resultsRow;

        resultsRow = currentResults;

        int nbLoops = resultsRow.length;

        int cpt = 0;
        for( int resultRow : resultsRow){
            if (resultRow > 0) {
                cpt++;
            }
        }

        actionResults = new ActionResult[cpt];

        cpt = 0;
        for (int i = 0; i < nbLoops; i++) {
            if (resultsRow[i] > 0) {
                actionResults[cpt++] = new ActionResult(resultsRow[i], actionNamesString[i] + " (" + actionNamesAP[i] + " AP)");
            }
        }

        ArrayList<ActionResult> list = new ArrayList<ActionResult>();

        for (int i = 0; i < actionResults.length; i++) {
            list.add(i, actionResults[i]);
        }

        final ResultListViewAdapter adapter = new ResultListViewAdapter(getActivity(), R.layout.fragment_results, R.layout.myresult, list);

        ListView listviewresults = (ListView) v.findViewById(R.id.listViewResults2);
        listviewresults.setAdapter(adapter);

        setFonts(v);

        return v;
    }

    private void setFonts(View v){
        // Begin SetFonts
        // Font path
        String fontPathR = "Coda-Regular.ttf";

        // Loading Font Face
        Typeface tfCodaR = Typeface.createFromAsset(getResources().getAssets(), fontPathR);


        // Applying font
        ((TextView) v.findViewById(R.id.topmsg2)).setTypeface(tfCodaR);



        // End SetFonts
    }





}