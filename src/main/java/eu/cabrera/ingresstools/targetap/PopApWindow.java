package eu.cabrera.ingresstools.targetap;

import eu.cabrera.ideutils.IdeUtil;
import eu.cabrera.ideutils.wei.mark.standout.WeiStandOutWindow;
import eu.cabrera.ideutils.wei.mark.standout.constants.WeiStandOutFlags;
import eu.cabrera.ideutils.wei.mark.standout.ui.WeiWindow;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PopApWindow extends WeiStandOutWindow implements OnClickListener {

    private final static String MY_ACTION_AP = "eu.cabrera.ingresstools.targetap.ap";


    private Button btn_send_ap;

    private EditText edit_ap;

    private ImageView btn_close ;

    private final static String TAG = "poapwindow" ;

	@Override
	public void createAndAttachView(final int windowId, FrameLayout frame) {

        View top, fenetre ;
        final int maxWidth = 0 ;
        int maxHeight = 0 ;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.popapwindow, frame, true);

		edit_ap = (EditText) view.findViewById(R.id.edit_ap);

		btn_send_ap = (Button) view.findViewById(R.id.btn_send_ap);


        TextView title = (TextView) view.findViewById(R.id.title);

        String fontPathR = "Coda-Regular.ttf";

        // Loading Font Face
        Typeface tfCodaR = Typeface.createFromAsset(getAssets(), fontPathR);

        // Applying font
        edit_ap.setTypeface(tfCodaR);
        btn_send_ap.setTypeface(tfCodaR);

        title.setTypeface(tfCodaR);

        // First calculate size of vertical components

		btn_send_ap.setOnClickListener(this) ;

        btn_close = (ImageView) view.findViewById(R.id.img_close);
        btn_close.setOnClickListener(this) ;

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> infos = getPackageManager().queryIntentActivities(intent, 0);

        for (ResolveInfo info : infos) {
            if ("com.nianticproject.ingress".equals(info.activityInfo.packageName)) {
                // We've got main activity class name
                intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                startActivity(intent);
                break ;
            }
        }

        // TODO : Modifier le message de la popup et changer sa taille
        // Si ingress n'est pas trouvé, afficher un message Alert


        fenetre = view.findViewById(R.id.win);
        top = view.findViewById(R.id.top);

        fenetre.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        //Log.d(TAG, "Hauteur content height = " + fenetre.getMeasuredHeight());

        top.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        //Log.d(TAG,"Hauteur content height = " + top.getMeasuredHeight());

        WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        if (getWindow(windowId) != null)
                getWindow(windowId).edit().setSize(Math.min(IdeUtil.dpToPx(Constants.AP_POPUP_MAX_WIDTH, window), maxWidth - 10), fenetre.getMeasuredHeight()+ IdeUtil.dpToPx(25, window)).commit();

    }

	@Override
	public StandOutLayoutParams getParams(int id, WeiWindow window) {

        WindowManager window2 = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        window2.getDefaultDisplay().getMetrics(displaymetrics);
        //int maxHeight = displaymetrics.heightPixels;
        int maxWidth = displaymetrics.widthPixels;


		return new StandOutLayoutParams(id, Math.min(IdeUtil.dpToPx(Constants.AP_POPUP_MAX_WIDTH, window2), maxWidth - 10), IdeUtil.dpToPx(Constants.AP_POPUP_MAX_HEIGHT, window2),
				StandOutLayoutParams.CENTER, StandOutLayoutParams.CENTER);
	}

	@Override
	public String getAppName() {
		return getResources().getString(R.string.app_name);
	}

	@Override
	public int getThemeStyle() {
		return android.R.style.Theme_Light;
	}

    @Override
    public int getFlags(int id) {
        return
                //    | WeiStandOutFlags.FLAG_DECORATION_SYSTEM
                WeiStandOutFlags.FLAG_BODY_MOVE_ENABLE
                | WeiStandOutFlags.FLAG_WINDOW_FOCUS_INDICATOR_DISABLE
                //	| WeiStandOutFlags.FLAG_WINDOW_HIDE_ENABLE
                //	| WeiStandOutFlags.FLAG_WINDOW_BRING_TO_FRONT_ON_TAP
                //  | WeiStandOutFlags.FLAG_WINDOW_EDGE_LIMITS_ENABLE
                //	| WeiStandOutFlags.FLAG_WINDOW_PINCH_RESIZE_ENABLE
                ;
    }

    @Override
    public String getPersistentNotificationTitle(int id) {
        return getResources().getString(R.string.popnotiftitle);
    }

    @Override
    public String getPersistentNotificationMessage(int id) {
        return getResources().getString(R.string.popnotifsubtitle);
    }

    @Override
    public Intent getPersistentNotificationIntent(int id) {
        return WeiStandOutWindow.getCloseIntent(this, PopApWindow.class, id);
    }

    @Override
    public int getAppIcon() {
        return android.R.drawable.ic_menu_close_clear_cancel;
    }

    @Override
    public void onClick(View v) {
        if (v == btn_send_ap) {
            String text = edit_ap.getText().toString();
            int popAp = 0 ;
            if (!text.equals("")) popAp = Integer.valueOf(text) ;

            Intent intent = new Intent();
            intent.setAction(MY_ACTION_AP);

            intent.putExtra("AP", popAp);

            sendBroadcast(intent);
        }

        if (v == btn_close){
            /*Intent intent = new Intent();
            intent.setAction(MY_ACTION_CLOSE);
            intent.putExtra("CLOSEMEAP", "CLOSEMEAP");

            sendBroadcast(intent);*/
            if (isExistingId(Constants.WEI_TARGETAP_AP_WINDOW_ID))
                close(Constants.WEI_TARGETAP_AP_WINDOW_ID);
            else {
                Log.d(TAG, "Does not exists " + Constants.WEI_TARGETAP_AP_WINDOW_ID);
            }


        }
    }

    @Override
    public void onReceiveData(int id, int requestCode, Bundle data, Class toto, int fromid){
        //Log.d(TAG,"Receive Data id=" + id + ",reqcode=" + requestCode);

        if (requestCode == Constants.WEI_DATAMSG_REQCODE_SHOW){
            int sid = data.getInt("id");
            //Log.d(TAG,"Rceived show " + sid);

            if (!isExistingId(sid))
                show(sid);
            else {
                Log.d(TAG, "Already exists " + sid);
            }
        }

        if (requestCode == Constants.WEI_DATAMSG_REQCODE_HIDE){
            int sid = data.getInt("id");
            //Log.d(TAG,"Rceived hide " + sid);

            if (isExistingId(sid))
                close(sid);
            else {
                Log.d(TAG, "Does not exists " + sid);
            }
        }
    }

}