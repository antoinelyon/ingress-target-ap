package eu.cabrera.ingresstools.targetap;


import android.content.Context;
import android.content.Intent;

import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.cabrera.ideutils.IdeUtil;
import eu.cabrera.ideutils.wei.mark.standout.WeiStandOutWindow;
import eu.cabrera.ideutils.wei.mark.standout.constants.WeiStandOutFlags;
import eu.cabrera.ideutils.wei.mark.standout.ui.WeiWindow;

import eu.cabrera.ingresstools.targetap.datastructures.PopActionResult;
import eu.cabrera.ingresstools.targetap.datastructures.PopResultListViewAdapter;

public class PopSolutionWindow extends WeiStandOutWindow implements OnClickListener{

    private ArrayList<PopActionResult> popActionResultlist ;

     private ImageView btnmini, btnmax, btn_close ;

    private TextView initialApText ;
    private TextView targetApText ;

    private TextView apDiffText ;

    private final static String TAG = "popsolution" ;

    private int apDiff ;

    private boolean reduced = false ;

    private int windowId ;

    private int maxWidth = 0 ;
    private int maxHeight = 0 ;

    private View fenetre ;

	@Override
	public void createAndAttachView(final int id, FrameLayout frame) {
        View view ;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.popsolutionwindow, frame, true);

        windowId = id;

        String fontPathR = "Coda-Regular.ttf";

        // Loading Font Face
        Typeface tfCodaR = Typeface.createFromAsset(getAssets(), fontPathR);

        // Applying font

        TextView title = (TextView) view.findViewById(R.id.titlesol);

        btn_close = (ImageView) view.findViewById(R.id.img_close);
        btnmini = (ImageView) view.findViewById(R.id.btnmini);
        btnmax = (ImageView) view.findViewById(R.id.btnmax);
        initialApText = (TextView)view.findViewById(R.id.popresinitial);
        targetApText = (TextView)view.findViewById(R.id.poprestarget);
        apDiffText = (TextView)view.findViewById(R.id.popresaptogoal);

        title.setTypeface(tfCodaR);
        initialApText.setTypeface(tfCodaR);
        targetApText.setTypeface(tfCodaR);
        apDiffText.setTypeface(tfCodaR);

        btn_close.setOnClickListener(this) ;
        btnmini.setOnClickListener(this) ;
        btnmax.setOnClickListener(this) ;



        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> infos = getPackageManager().queryIntentActivities(intent, 0);

        for (ResolveInfo info : infos) {
            if ("com.nianticproject.ingress".equals(info.activityInfo.packageName)) {
                // We've got main activity class name
                intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                startActivity(intent);
                break ;
            }
        }
        popActionResultlist = new ArrayList<PopActionResult>();

        final PopResultListViewAdapter adapter = new PopResultListViewAdapter(this, R.layout.popsolutionwindow, R.layout.popresult, popActionResultlist, this);

        ListView listviewpopresults = (ListView) view.findViewById(R.id.listViewPopResults);
        listviewpopresults.setAdapter(adapter);



        fenetre = view.findViewById(R.id.win);
   }


	@Override
	public StandOutLayoutParams getParams(int id, WeiWindow window) {
        int screenWidth ;
        int screenHeight ;

        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
         screenHeight = displaymetrics.heightPixels;
         screenWidth = displaymetrics.widthPixels;


        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");

        int statusHeight = getResources().getDimensionPixelSize(resourceId);

        //Log.d (TAG,"Width = " + screenWidth + ", Height = " + screenHeight) ;
        //Log.d (TAG,"in dp = " + IdeUtils.dpToPx(screenWidth, windowManager) + "," + IdeUtils.dpToPx(screenHeight, windowManager));
        //Log.d(TAG,"Size = " + Math.min(IdeUtils.dpToPx(x, windowManager), screenWidth - IdeUtils.dpToPx(10, windowManager)) + "," + Math.min(IdeUtils.dpToPx(y, windowManager), screenHeight /2 - IdeUtils.dpToPx(10+statusHeight, windowManager)));

        maxWidth =  Math.min(IdeUtil.dpToPx(Constants.SOL_POPUP_MAX_WIDTH, windowManager), screenWidth - IdeUtil.dpToPx(10, windowManager)) ;
        maxHeight = Math.min(IdeUtil.dpToPx(Constants.SOL_POPUP_MAX_HEIGHT, windowManager), screenHeight / 2 - IdeUtil.dpToPx(10 + statusHeight, windowManager)) ;

        return new StandOutLayoutParams(id, maxWidth, maxHeight,
				StandOutLayoutParams.CENTER, StandOutLayoutParams.CENTER);
	}

	@Override
	public String getAppName() {
		return getResources().getString(R.string.app_name);
	}

	@Override
	public int getThemeStyle() {
		return android.R.style.Theme_Light;
	}

    @Override
    public void onReceiveData(int id, int requestCode, Bundle data, Class toto, int fromid){

        //Log.d(TAG,"Receive Data id=" + id + ",reqcode=" + requestCode);

        if (requestCode == Constants.WEI_DATAMSG_REQCODE_SHOW){
            int sid = data.getInt("id");
            //Log.d(TAG,"Rceived show " + sid);

            if (!isExistingId(sid))
                show(sid);
            else {
                Log.d(TAG, "Already exists " + sid);
            }

            int[] currentResults;

            int initialAp ;
            int targetAp ;

            String[] actionNamesString ;
            int[] actionNamesAP ;

            currentResults = data.getIntArray("current_results");
            initialAp = data.getInt("initial_ap",0);
            targetAp = data.getInt("target_ap",0);

            apDiff = targetAp - initialAp ;

            actionNamesString = data.getStringArray("action_names");
            actionNamesAP = data.getIntArray("action_ap");


            PopActionResult[] popActionResults;

            int[] resultsRow;

            resultsRow = currentResults;

            int nbLoops = resultsRow.length;

            int cpt = 0;
            for( int resultRow : resultsRow){
                if (resultRow > 0) {
                    cpt++;
                }
            }

            popActionResults = new PopActionResult[cpt];

            cpt = 0;
            for (int i = 0; i < nbLoops; i++) {
                if (resultsRow[i] > 0) {
                    popActionResults[cpt++] = new PopActionResult(resultsRow[i], actionNamesString[i], actionNamesAP[i]);
                }
            }

            popActionResultlist.clear();

            for (int i = 0; i < popActionResults.length; i++) {
                popActionResultlist.add(i, popActionResults[i]);
            }

            initialApText.setText(getResources().getText(R.string.valeur_depart) + " : " + + initialAp);
            targetApText.setText(getResources().getText(R.string.valeur_arrivee) + " : " + + targetAp);
            apDiffText.setText(getResources().getText(R.string.apdifference) + " : " + apDiff);
        }

        if (requestCode == Constants.WEI_DATAMSG_REQCODE_HIDE){
            int sid = data.getInt("id");
            Log.d(TAG,"Rceived hide " + sid);

            if (isExistingId(sid))
                close(sid);
            else
                Log.d(TAG,"Does not exists " + sid) ;
        }


    }

    @Override
    public String getPersistentNotificationTitle(int id) {
        return getResources().getString(R.string.popresnotiftitle) ;//getResources().getString(R.string.popnotiftitle);
    }

    @Override
    public String getPersistentNotificationMessage(int id) {
        return getResources().getString(R.string.popresnotifsubtitle) ; //getResources().getString(R.string.popnotifsubtitle);
    }

    @Override
    public Intent getPersistentNotificationIntent(int id) {
        return WeiStandOutWindow.getCloseIntent(this, PopSolutionWindow.class, id);
    }

    @Override
    public int getAppIcon() {
        return android.R.drawable.ic_menu_close_clear_cancel;
    }

    @Override
    public void onClick(View v){

        if (v == btnmini) {
            reduced = true;
            btnmini.setVisibility(View.GONE);
            btnmax.setVisibility(View.VISIBLE);
            changesize();
        }
        if (v == btnmax) {
            reduced = false;
            btnmax.setVisibility(View.GONE);
            btnmini.setVisibility(View.VISIBLE);
            changesize();
        }
        if (v == btn_close){
            /*
            Intent intent = new Intent();
            intent.setAction(MY_ACTION_CLOSE);
            intent.putExtra("CLOSEMERES", "CLOSEMERES");

            sendBroadcast(intent);*/
            if (isExistingId(Constants.WEI_TARGETAP_RESULTS_WINDOW_ID))
                close(Constants.WEI_TARGETAP_RESULTS_WINDOW_ID);
            else {
                Log.d(TAG, "Does not exists " + Constants.WEI_TARGETAP_RESULTS_WINDOW_ID);
            }

        }
    }

    @Override
    public int getFlags(int id) {
        return
                //    | WeiStandOutFlags.FLAG_DECORATION_SYSTEM
                WeiStandOutFlags.FLAG_BODY_MOVE_ENABLE
                | WeiStandOutFlags.FLAG_WINDOW_FOCUS_INDICATOR_DISABLE
                //	| WeiStandOutFlags.FLAG_WINDOW_HIDE_ENABLE
                //	| WeiStandOutFlags.FLAG_WINDOW_BRING_TO_FRONT_ON_TAP
                //  | WeiStandOutFlags.FLAG_WINDOW_EDGE_LIMITS_ENABLE
                //	| WeiStandOutFlags.FLAG_WINDOW_PINCH_RESIZE_ENABLE
                ;
    }

    public void updateDiff(int diff){

        apDiff +=diff ;
        apDiffText.setText(getResources().getText(R.string.apdifference) + " : " + apDiff);
        //Log.d(TAG,"Update diff " + diff );
    }

    private void changesize(){
        WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        //Log.d(TAG,"Change size dptopx 25 = " + IdeUtils.dpToPx(25,window) + " " + IdeUtils.dpToPx(x, window) + " " + (screenWidth - 10));
        fenetre.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        if (getWindow(windowId) != null) {
            if (reduced)
                getWindow(windowId).edit().setSize(maxWidth, IdeUtil.dpToPx(25, window)).commit();
            else
                getWindow(windowId).edit().setSize(maxWidth, maxHeight).commit();
        }
    }



}