package eu.cabrera.ingresstools.targetap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 *
 * Created by antoine on 10/06/2014.
 */
public class ReceiveCloseMeRes extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent activity = new Intent();
        activity.setClass(context, ResultActivity.class);
        activity.putExtra("CLOSEMERES", intent.getStringExtra("CLOSEMERES"));
        activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(activity);
    }
}
