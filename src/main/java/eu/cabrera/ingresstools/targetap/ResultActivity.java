package eu.cabrera.ingresstools.targetap;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

import eu.cabrera.ideutils.TypefaceSpan;
import eu.cabrera.ideutils.wei.mark.standout.WeiStandOutWindow;
import eu.cabrera.ingresstools.targetap.datastructures.ActionAp;
import eu.cabrera.ingresstools.targetap.datastructures.DataInWrapper;
import eu.cabrera.ingresstools.targetap.datastructures.IndexPair;
import eu.cabrera.ingresstools.targetap.datastructures.MyFragmentPagerAdapter;
import eu.cabrera.ingresstools.targetap.datastructures.ResultsPerActionList;

// Swipe gauche droite pour les solutions alternatives avec autant d'actions
// Swipe haut bas pour avoir plus ou moins d'actions, vers le bas : moins
// Double touch pour verouiller une solution ?
// La meilleure solution qui est disponible avant 5s est la solution de référence, on n'affiche pas celles en dessous
// + et - pour la progression dans la solution avec mise à jour de l'ap initiale "virtuelle"
// quand on a mis à jour l'ap initiale virtuelle avec +/-, on peut demander, find alternative solutions pour relancer le calcul
// avec les mêmes options ? ou retour activity main ?

// Pop sur l'application Ingress pour la saisie de l'ap
public class ResultActivity extends ActivityAbstractor implements View.OnClickListener {

    private CalcEngine calcEngine;

    private ActionAp[] actionNames;

    private int minimumActions = 0;
    private boolean isAlreadyOld = false;

    private final ResultsPerActionList resultsPerActionList = new ResultsPerActionList();

    private MyFragmentPagerAdapter pagerAdapter ;
    private ViewPager pager ;
    private FragmentManager fm ;

    private boolean resultsAlreadyShown = false;

    private boolean destroyedActivity = false ;

    private LinearLayout blocMsg ;
    private Button buttonshowpop ;

    private int initialAp;
    private int targetAp;

    final static String TAG ="result_activity" ;

    private TextView differenceOfAp ;
    private TextView depart;
    private TextView arrivee;

    @Override
    public void onStart() {
        super.onStart();
        // The rest of your onStart() code.
        EasyTracker.getInstance(this).activityStart(this);  // GAnalytics.
    }

    @Override
    public void onStop() {
        super.onStop();
        // The rest of your onStop() code.
        EasyTracker.getInstance(this).activityStop(this);  // GAnalytics.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (calcEngine != null) {
            destroyedActivity = true ;
            calcEngine.stopCalcultation();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.result_activity, menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up btn_paypal, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_about:
                openAbout();
                return true;
            case R.id.action_help:
                openHelp();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void openHelp() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        //Log.d(TAG,"onCreate") ;

        setFonts();

        depart = (TextView) findViewById(R.id.depart);
        arrivee = (TextView) findViewById(R.id.arrivee);

        buttonshowpop = (Button) findViewById(R.id.buttonshowpop) ;
        buttonshowpop.setOnClickListener(this);
        differenceOfAp = (TextView) findViewById(R.id.differenceofap);

        Intent intent = getIntent();
        processResIntent(intent) ;

    }


    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return calcEngine;
    }

    @Override
    public void showResults() {
        //Log.d("Results","ShowResults");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Log.d("Results","nb=" + resultsPerActionList.getTotal());
                pagerAdapter = new MyFragmentPagerAdapter(fm, resultsPerActionList, actionNames);
                pager.setAdapter(pagerAdapter);
                pagerAdapter.notifyDataSetChanged();

                ViewPager pager = (ViewPager) findViewById(R.id.pager);

                pager.setVisibility(View.VISIBLE);

                Button pop = (Button) findViewById(R.id.buttonshowpop);
                pop.setVisibility(View.VISIBLE);
            }
        });

        resultsAlreadyShown = true;
    }

    @Override
    public void updateResults(final int totalActions, final int[] indexes) {

        synchronized (resultsPerActionList) {
            //Log.d("Results", "nb in update =" + resultsPerActionList.getTotal());
            resultsPerActionList.update(totalActions, indexes);
        }

        if ((isAlreadyOld && totalActions < minimumActions) || !isAlreadyOld) {
            minimumActions = totalActions;
            isAlreadyOld = true;
        }

    }

    @Override
    synchronized public void updateProgressBar(final int progress) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

                progressBar.setProgress(progress);
            }
        });

    }

    @Override
    synchronized public void updateProgressMsg(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView progressMsg = (TextView) findViewById(R.id.progressMsg);

                progressMsg.setText(msg);
            }
        });
    }

    private  void hideProgressBar(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);

                progressBar = (ProgressBar) findViewById(R.id.animatedBar);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private  void hideProgressMsg(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView progressMsg = (TextView) findViewById(R.id.progressMsg);

                progressMsg.setVisibility(View.GONE);
            }
        });
    }

    private  void showNoSolution(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView nsol = (TextView)findViewById(R.id.nosolution);
                nsol.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    synchronized public void calcFinished(boolean interrupted, boolean solOk) {

        if (!destroyedActivity) {
            hideProgressBar();

            if (solOk) {

                if (!resultsAlreadyShown) {
                    showResults();
                }

                String msg;

                if (!interrupted) {

                    msg = getResources().getString(R.string.msg_optimal_solution);

                } else {

                    msg = getResources().getString(R.string.msg_interrupted_no_better_solution);

                }

                if (resultsPerActionList.nbTotal >1) msg += "\n" + getResources().getString(R.string.msg_swipe);

                // Log.d("res", "nbdisp=" + nbActionsDisplayed + ", mini=" + minimumActions);
                updateProgressMsg(msg);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                            pagerAdapter = new MyFragmentPagerAdapter(fm, resultsPerActionList, actionNames);

                            pager.setAdapter(pagerAdapter);
                            pagerAdapter.notifyDataSetChanged();

                    }
                });


            } else {
                hideProgressMsg();
                showNoSolution();
            }

        }
        calcEngine = null;
    }

    private void setFonts(){
        // Begin SetFonts
        // Font path
        String fontPathR = "Coda-Regular.ttf";

        // Loading Font Face
        Typeface tfCodaR = Typeface.createFromAsset(getAssets(), fontPathR);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            SpannableString s = new SpannableString(getResources().getString(R.string.title_result));
            s.setSpan(new TypefaceSpan(this, fontPathR), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ActionBar actionBar = getActionBar();
            if (actionBar != null) actionBar.setTitle(s);
        }

        // Applying font
        ((TextView) findViewById(R.id.introrecap)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.depart)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.arrivee)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.differenceofap)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.nosolution)).setTypeface(tfCodaR);
        ((TextView) findViewById(R.id.progressMsg)).setTypeface(tfCodaR);
        ((Button) findViewById(R.id.buttonshowpop)).setTypeface(tfCodaR);

        // End SetFonts
    }

    @Override
    public void onClick(View v){
        if (v == blocMsg){
            if (calcEngine != null) {
                calcEngine.stopCalcultation();
            }
        }
        if (v == buttonshowpop){

                show() ;

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        processResIntent(intent);
    }

    private void processResIntent(Intent intent) {
        //Log.d(TAG,"Received intent" + intent) ;
        if (intent.hasExtra("CLOSEMERES")) {
            //Log.d(TAG,"Receive Intent CLOSEMERES");

            hide(); // WeiStandOutWindow.close(getApplicationContext(), MemoMapWindow.class, Constants.WEI_MEMOMAP_WINDOW_ID);

        }

        if (intent.hasExtra(Constants.EXTRA_MESSAGE)) {
            //Log.d(TAG,"Receive Intent EXTRA_MESSAGE");

            DataInWrapper dataInWrapper = (DataInWrapper) intent.getSerializableExtra(Constants.EXTRA_MESSAGE);

            initialAp = dataInWrapper.getActualAp();
            targetAp = dataInWrapper.getTargetAp();

            depart.setText(getResources().getString(R.string.valeur_depart) + " : " + initialAp);
            arrivee.setText(getResources().getString(R.string.valeur_arrivee) + " : " + targetAp);

            differenceOfAp.setText(getResources().getString(R.string.differenceofap) + " " + (targetAp - initialAp));

            actionNames = dataInWrapper.getActionAps();

            /** Getting a reference to the ViewPager defined the layout file */
            pager = (ViewPager) findViewById(R.id.pager);

            /** Getting fragment manager */
            fm = getSupportFragmentManager();

            /** Instantiating FragmentPagerAdapter */
            pagerAdapter = new MyFragmentPagerAdapter(fm, resultsPerActionList, actionNames);

            /** Setting the pagerAdapter to the pager object */
            pager.setAdapter(pagerAdapter);

            blocMsg = (LinearLayout) findViewById(R.id.blocMsg);
            blocMsg.setOnClickListener(this) ;

            calcEngine = (CalcEngine) getLastNonConfigurationInstance();
            if (calcEngine == null) {
                calcEngine = new CalcEngine(initialAp, targetAp, actionNames, this);
                calcEngine.processCalculation();
            }
        }
    }

    private void show(){
        //Log.d(TAG,"show pop ap");
        Bundle data = new Bundle();
        data.putInt("id",Constants.WEI_TARGETAP_RESULTS_WINDOW_ID);
        int currentItem = pager.getCurrentItem();
        //Log.d(TAG,"Current item = " + currentItem);

        IndexPair indexPair = resultsPerActionList.indexToPair(currentItem);
        //Log.d(TAG, "indexPair=" + indexPair.x + ", " + indexPair.y);

        data.putIntArray("current_results", resultsPerActionList.getResults(indexPair.x, indexPair.y));

        data.putInt("initial_ap",initialAp);
        data.putInt("target_ap",targetAp);

        String[] names = new String[actionNames.length];
        int[] ap = new int[actionNames.length];
        for (int i = 0; i < actionNames.length; i++) {
            names[i] = actionNames[i].getActionName();
            ap[i] = actionNames[i].getAp();
        }
        data.putStringArray("action_names", names);
        data.putIntArray("action_ap", ap);
        WeiStandOutWindow.sendData(getApplicationContext(), PopSolutionWindow.class, WeiStandOutWindow.DISREGARD_ID,
                Constants.WEI_DATAMSG_REQCODE_SHOW, data, null, WeiStandOutWindow.DISREGARD_ID);
    }

    private void hide(){
        //Log.d(TAG,"hide pop ap");
        Bundle data = new Bundle();
        data.putInt("id",Constants.WEI_TARGETAP_RESULTS_WINDOW_ID);
        WeiStandOutWindow.sendData(getApplicationContext(), PopSolutionWindow.class, WeiStandOutWindow.DISREGARD_ID,
                Constants.WEI_DATAMSG_REQCODE_HIDE, data, null, WeiStandOutWindow.DISREGARD_ID);
    }
}
