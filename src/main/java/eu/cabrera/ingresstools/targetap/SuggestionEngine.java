package eu.cabrera.ingresstools.targetap;

import java.util.ArrayList;
import java.util.Collections;

import eu.cabrera.ingresstools.targetap.datastructures.PatternNumber;

/**
 * Suggestion Engine for Target AP
 * Created by antoine on 20/05/2014.
 */
final class SuggestionEngine {

    private final ArrayList<PatternNumber> patterns = new ArrayList<PatternNumber>();

    public SuggestionEngine(){

    }

    ArrayList<PatternNumber> getPatternNumbers (int actualAp){
        patterns.clear();

        // Level like patterns

        addIfOk(actualAp,    "2500",R.string.pattern_level2,true) ;
        addIfOk(actualAp,   "20000",R.string.pattern_level3,true); // L3
        addIfOk(actualAp,   "70000",R.string.pattern_level4,true); // L4
        addIfOk(actualAp,  "150000",R.string.pattern_level5,true); // L5
        addIfOk(actualAp,  "300000",R.string.pattern_level6,true); // L6
        addIfOk(actualAp,  "600000",R.string.pattern_level7,true); // L7
        addIfOk(actualAp, "1200000",R.string.pattern_level8,true); // L8
        addIfOk(actualAp, "2400000",R.string.pattern_level9,true); // L9*
        addIfOk(actualAp, "4000000",R.string.pattern_level10,true); // L10
        addIfOk(actualAp, "6000000",R.string.pattern_level11,true); // L11
        addIfOk(actualAp, "8400000",R.string.pattern_level12,true); // L12
        addIfOk(actualAp,"12000000",R.string.pattern_level13,true); // L13
        addIfOk(actualAp,"17000000",R.string.pattern_level14,true); // L14
        addIfOk(actualAp,"24000000",R.string.pattern_level15,true); // L15
        addIfOk(actualAp,"40000000",R.string.pattern_level16,true); // L16

        // Generate same number patterns
        int baseSameDigitsPattern = 111111111 ; // Should be enough with more than 100M
        addIfOk(actualAp,""+baseSameDigitsPattern,R.string.pattern_samedigits,false);
        for (int i =2;i<=9;i++) addIfOk(actualAp,""+(baseSameDigitsPattern*i),R.string.pattern_samedigits,false);

        // Generate 0 ending patterns after 1M
        for (int i=0;i<100;i++) {
            if (i!=4 && i!=6 && i!=12 && i!=17 && i!=24 && i!=40 ) addIfOk(actualAp, "" + i + "000000", R.string.pattern_1mmultiple, true);
        }

        if( actualAp > 10000) {
            // Ascending suite patterns
            addIfOk(actualAp, "1234567890", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "2345678901", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "3456789012", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "4567890123", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "5678901234", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "6789012345", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "7890123456", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "8901234567", R.string.pattern_ascendingsuites, false);
            addIfOk(actualAp, "9012345678", R.string.pattern_ascendingsuites, false);

            // Descending suite patterns
            addIfOk(actualAp, "9876543210", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "8765432109", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "7654321098", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "6543210987", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "5432109876", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "4321098765", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "3210987654", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "2109876543", R.string.pattern_descendingsuites, false);
            addIfOk(actualAp, "1098765432", R.string.pattern_descendingsuites, false);
        }
        // Special numbers patterns
        addIfOk(actualAp,"3141592653",R.string.pattern_pi,false) ; // Pi 3,1415...
        addIfOk(actualAp,"1618033988",R.string.pattern_gold,false) ; // Nombre d'or 1,618...

        // Hors patterns
        // Nombres palindromes ?
        // Ajout de patterns à la con : 12121212 ?
        // Ajout de patterns qui lus à l'envers donnent un mot ? (là je fume)

        Collections.sort(patterns);

        return patterns ;
    }


    private void addIfOk(int actualAp, String pattern, int resId, boolean exactPat){
        String actualAPString = Integer.toString(actualAp);
        int actualApSize = actualAPString.length();

        if (actualApSize < Constants.MIN_SUGGEST_SIZE) actualApSize = Constants.MIN_SUGGEST_SIZE ;

        if (!exactPat) {
            String sub1 = pattern.substring(0, actualApSize);
            String sub2 = pattern.substring(0, actualApSize+1);

            if (((Integer.parseInt(sub1)) > actualAp) && (Integer.parseInt(sub1)<actualAp+Constants.AP_MAXGAP_TO_SUGGESTED)) {
                if (!patterns.contains(new PatternNumber(sub1, resId))) {
                    patterns.add(new PatternNumber(sub1, resId));
                }
            }

            if (((Integer.parseInt(sub2)) > actualAp) && (Integer.parseInt(sub2)<actualAp+Constants.AP_MAXGAP_TO_SUGGESTED)) {
                if (!patterns.contains(new PatternNumber(sub2, resId))) {
                    patterns.add(new PatternNumber(sub2, resId));
                }
            }

        }
        else {
            if (((Integer.parseInt(pattern)) > actualAp) && (Integer.parseInt(pattern)<actualAp+Constants.AP_MAXGAP_TO_SUGGESTED) && pattern.length()>=Constants.MIN_SUGGEST_SIZE) {
                if (!patterns.contains(new PatternNumber(pattern, resId))) {
                    patterns.add(new PatternNumber(pattern, resId));
                }
            }
        }
    }

}
