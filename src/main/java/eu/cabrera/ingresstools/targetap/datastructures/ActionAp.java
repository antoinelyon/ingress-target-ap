package eu.cabrera.ingresstools.targetap.datastructures;

import java.io.Serializable;

/**
 * Structure accueuillant les intitulés d'actions, valeurs d'ap, et état sélectionné ou non
 * Created by antoine on 06/04/2014.
 */
public class ActionAp implements Serializable {

    private final String actionName ;
    public final int ap ;
    private boolean status ;

    public ActionAp(String actionName, int ap, boolean status){
        this.actionName = actionName ;
        this.ap = ap ;
        this.status = status ;
    }

    public void setStatus(boolean status){
        this.status = status ;
    }

    public int getAp(){
        return this.ap ;
    }
    
    public String getActionName(){
        return this.actionName ;
    }

    public boolean getStatus() { return this.status ; }

}
