package eu.cabrera.ingresstools.targetap.datastructures;

/**
 * Structure acceuillant les résultats du calcul : nombre d'actions et intitulés
 *
 * Created by antoine on 18/04/2014.
 */
public class ActionResult {

    private final int nb ;
    private final String actionName ;

    public ActionResult(int nb, String actionName){
        this.nb = nb;
        this.actionName = actionName ;
    }

    public int getNb(){
        return this.nb ;
    }
    public String getActionName(){
        return this.actionName ;
    }

}