package eu.cabrera.ingresstools.targetap.datastructures;

import java.io.Serializable;

/**
 * Bunch of inputed data : actions options, actual & target AP sent to ResultActivity for calculation & display
 * Created by antoine on 09/05/2014.
 */
public class DataInWrapper implements Serializable {
    private final int actualAp ;
    private final int targetAp ;
    private final ActionAp[] actionAps ;

    public DataInWrapper (int actualAp, int targetAp, ActionAp[] actionAps) {
        this.actionAps = actionAps;
        this.actualAp = actualAp;
        this.targetAp = targetAp;
    }

    public ActionAp[] getActionAps(){
        return this.actionAps ;
    }
    public int getActualAp() {return actualAp ;}
    public int getTargetAp() {return targetAp ;}

}
