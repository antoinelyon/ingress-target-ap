package eu.cabrera.ingresstools.targetap.datastructures;

/**
 *
 * Created by antoine on 27/05/2014.
 */
public class IndexPair {
    public final int x;
    public final int y ;

    public IndexPair(int x, int y){
        this.x = x ;
        this.y = y ;
    }
}
