package eu.cabrera.ingresstools.targetap.datastructures;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import eu.cabrera.ingresstools.targetap.MyFragment;

public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private final ResultsPerActionList resultsPerActionList ;
    private final ActionAp[] actionNames ;
    private int pagecount = 0;

    /** Constructor of the class */
    public MyFragmentPagerAdapter(FragmentManager fm, ResultsPerActionList resultsPerActionList, ActionAp[] actionNames) {
        super(fm);
        this.resultsPerActionList = resultsPerActionList ;
        this.actionNames = actionNames ;

        // First time get number of solutions
        pagecount = resultsPerActionList.getTotal() ;
    }

    /** This method will be invoked when a page is requested to create */
    @Override
    public Fragment getItem(int currentId) {
        //Log.d("MyFrag","retreive " + currentId) ;

        MyFragment myFragment = new MyFragment();
        Bundle data = new Bundle();
        data.putInt("current_page", currentId);

        synchronized (resultsPerActionList){

            data.putInt("nb_sol", pagecount);
            //Log.d("Myfrag","pagecount=" + pagecount);
            if (resultsPerActionList.getTotal() > 0) {
                IndexPair indexPair = resultsPerActionList.indexToPair(currentId);
                //Log.d("MyFrag", "indexPair=" + indexPair.x + ", " + indexPair.y);

                data.putInt("current_nb", resultsPerActionList.getNb(indexPair.x));
                data.putInt("current_actions", resultsPerActionList.getActions(indexPair.x));
                data.putIntArray("current_results", resultsPerActionList.getResults(indexPair.x, indexPair.y));

                String[] names = new String[actionNames.length];
                int[] ap = new int[actionNames.length];
                for (int i = 0; i < actionNames.length; i++) {
                    names[i] = actionNames[i].getActionName();
                    ap[i] = actionNames[i].getAp();
                }
                data.putStringArray("action_names", names);
                data.putIntArray("action_ap", ap);
            }
        }

        myFragment.setArguments(data);
        return myFragment;
    }

    /** Returns the number of pages */
    @Override
    public int getCount() {
        //Log.d("MyFrag","getcount=" + pagecount);
        return pagecount;

    }

}