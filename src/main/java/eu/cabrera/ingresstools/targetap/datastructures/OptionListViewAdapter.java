package eu.cabrera.ingresstools.targetap.datastructures;

import android.content.Context;

import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;


import java.util.List;

import eu.cabrera.ingresstools.targetap.R;

/**
 * Permet de mapper la liste d'options affichée avec la structure de données
 *
 * Created by antoine on 18/04/2014.
 */
public class OptionListViewAdapter extends ArrayAdapter  {

    private final Context mContext;
    private final List<ActionAp> mMyComplexClassList;
    private final LayoutInflater inflater;

    public OptionListViewAdapter(Context context, int resource,
                                 int customResourceId, List<ActionAp> objects) {
        super(context, resource, customResourceId, objects);
        this.mContext = context;
        this.mMyComplexClassList = objects;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mMyComplexClassList.size();
    }
    public ActionAp getItem(int position) {
        return this.mMyComplexClassList.get(position);
    }

    public long getItemId(int position) {
        return  position;
    }

    public static class ViewHolder{
        public CheckBox checkBox;
        public TextView text1;
    }

    @Override
    public View getView(int pos, View inView, ViewGroup parent) {
        ViewHolder holder;
        // If row doesn't exist > we make it
        if (inView == null)
        {
            inView = this.inflater.inflate(R.layout.mylist,parent,false);
            holder = new ViewHolder();
            // We get views
            holder.checkBox = (CheckBox) inView.findViewById(R.id.checkBox);
            holder.text1 = (TextView) inView.findViewById(R.id.text1);
            inView.setTag(holder);

            String fontPathR = "Coda-Regular.ttf";
            Typeface tfCodaR = Typeface.createFromAsset(mContext.getAssets(), fontPathR);
            holder.text1.setTypeface(tfCodaR);
        }

        // Else we get row which is in memory
        else
            holder = (ViewHolder) inView.getTag();

        // We get the current object
        ActionAp myComplexClass = this.mMyComplexClassList.get(pos);

        // We update views
        holder.checkBox.setChecked(myComplexClass.getStatus());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            holder.text1.setActivated(myComplexClass.getStatus());
        }
        else {
            holder.text1.setSelected(myComplexClass.getStatus());
        }

        /*String msg = "++ cb checked = " + holder.checkBox.isChecked() + " tx selected = " + holder.text1.isSelected() ;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) msg += " activated = " + holder.text1.isActivated() ;*/

        holder.text1.setText(myComplexClass.getActionName() + " (" + myComplexClass.getAp() + " AP) ");

        return (inView);
    }

}
