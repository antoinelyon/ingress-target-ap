package eu.cabrera.ingresstools.targetap.datastructures;

/**
 *
 * Created by antoine on 19/05/2014.
 */

public class PatternNumber implements Comparable<PatternNumber>{
    private final String number ;
    private final int nameResId ;


    public PatternNumber(String number, int nameResId){
        this.number = number ;
        this.nameResId = nameResId ;
    }

    public String getNumber (){
        return number ;
    }

    public int getNameResId(){
        return nameResId ;
    }

    @Override
    public int compareTo(PatternNumber PatternNumber){
        return Integer.parseInt(this.number)-Integer.parseInt(PatternNumber.getNumber());
    }

}
