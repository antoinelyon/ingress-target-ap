package eu.cabrera.ingresstools.targetap.datastructures;

/**
 * Structure acceuillant les résultats du calcul : nombre d'actions et intitulés
 *
 * Created by antoine on 18/04/2014.
 */
public class PopActionResult {

    private final int nb ;
    private final String actionName ;
    private final int ap ;
    private int currentNb ;

    public PopActionResult(int nb, String actionName, int ap){
        this.nb = nb;
        this.actionName = actionName ;
        this.ap = ap ;
        this.currentNb = 0 ;
    }

    public int getNb(){
        return this.nb ;
    }
    public String getActionName(){
        return this.actionName ;
    }
    public int getAp(){
        return this.ap ;
    }

    public int getCurrentNb (){
        return this.currentNb ;
    }
    public void setCurrentNb(int currentNb){
        this.currentNb = currentNb;
    }

}