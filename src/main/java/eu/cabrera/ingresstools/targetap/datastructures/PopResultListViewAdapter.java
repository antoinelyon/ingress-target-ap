package eu.cabrera.ingresstools.targetap.datastructures;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import eu.cabrera.ingresstools.targetap.PopSolutionWindow;
import eu.cabrera.ingresstools.targetap.R;

/**
 * Permet de mapper les résultats affichés avec la structure de résultats
 *
 * Created by antoine on 18/04/2014.
 */
public class PopResultListViewAdapter extends ArrayAdapter {

    private final Context mContext;
    private final List<PopActionResult> popActionResultList;
    private final LayoutInflater inflater;

    private final PopSolutionWindow caller ;

    public PopResultListViewAdapter(Context context, int resource,
                                    int textViewResourceId, List<PopActionResult> popActionResultList, PopSolutionWindow caller) {
        super(context, resource, textViewResourceId, popActionResultList);
        this.mContext = context;
        this.popActionResultList = popActionResultList;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.caller = caller ;
    }

    public int getCount() {
        return popActionResultList.size();
    }
    public PopActionResult getItem(int position) {
        return this.popActionResultList.get(position);
    }

    public long getItemId(int position) {
        return  position;
    }

    public static class ViewHolder{
        TextView name;
        TextView nb;
        Button btnMore ;
        Button btnLess ;
    }

    @Override
    public View getView(int pos,  View inView, ViewGroup parent) {
        final ViewHolder holder;
        // Si la ligne n'éxiste pas > On créée la ligne
        // If row doesn't exist > we make it
        if (inView == null)
        {
            inView = this.inflater.inflate(R.layout.popresult,parent,false);
            holder = new ViewHolder();
            // On affecte les views
            // We get views
            holder.name = (TextView)  inView.findViewById(R.id.name);
            holder.nb = (TextView) inView.findViewById(R.id.nb);
            holder.btnLess = (Button) inView.findViewById(R.id.less);
            holder.btnMore = (Button) inView.findViewById(R.id.more);
            inView.setTag(holder);

            String fontPathR = "Coda-Regular.ttf";
            Typeface tfCodaR = Typeface.createFromAsset(mContext.getAssets(), fontPathR);
            holder.name.setTypeface(tfCodaR);
            holder.nb.setTypeface(tfCodaR);
            holder.btnLess.setTypeface(tfCodaR);
            holder.btnMore.setTypeface(tfCodaR);
        }
        // Sinon on récupère la ligne qui est en mémoire
        // Else we get row which is in memory
        else
            holder = (ViewHolder) inView.getTag();

        // On récupère l'objet courant
        // We get the current object
        final PopActionResult currentPopActionResult = this.popActionResultList.get(pos);

        // On met à jour nos views
        // We update views

        String fontPathR = "Coda-Regular.ttf";
        Typeface tfCodaR = Typeface.createFromAsset(mContext.getAssets(), fontPathR);
        holder.name.setTypeface(tfCodaR);
        holder.nb.setTypeface(tfCodaR);
        holder.btnLess.setTypeface(tfCodaR);
        holder.btnMore.setTypeface(tfCodaR);
        holder.name.setText(currentPopActionResult.getActionName() + " (" + currentPopActionResult.getAp() + ")");
        holder.nb.setText(Integer.toString(currentPopActionResult.getCurrentNb()) + " / " + Integer.toString(currentPopActionResult.getNb()) + "x");

        final View inView2 = inView ;
        holder.btnLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("adapt","******* LESS " + currentPopActionResult.getCurrentNb()) ;
                if (currentPopActionResult.getCurrentNb()>0) {
                    currentPopActionResult.setCurrentNb(currentPopActionResult.getCurrentNb() - 1);
                    holder.nb.setText(Integer.toString(currentPopActionResult.getCurrentNb()) + " / " + Integer.toString(currentPopActionResult.getNb()) + "x");
                    caller.updateDiff(-currentPopActionResult.getAp());
                }
                if (currentPopActionResult.getCurrentNb() < currentPopActionResult.getNb()) {
                    holder.name.setTextColor(Color.WHITE);
                    holder.nb.setTextColor(Color.WHITE);
                }

            }
        });
        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("adapt","******* MORE " + currentPopActionResult.getCurrentNb()) ;
                if (currentPopActionResult.getCurrentNb()<currentPopActionResult.getNb()) {
                    currentPopActionResult.setCurrentNb(currentPopActionResult.getCurrentNb() + 1);

                    holder.nb.setText(Integer.toString(currentPopActionResult.getCurrentNb()) + " / " + Integer.toString(currentPopActionResult.getNb()) + "x");
                    caller.updateDiff(currentPopActionResult.getAp());
                }
                if (currentPopActionResult.getCurrentNb() == currentPopActionResult.getNb() ) {

                    holder.name.setTextColor(inView2.getResources().getColor(R.color.actionachieved));
                    holder.nb.setTextColor(inView2.getResources().getColor(R.color.actionachieved));
                }


            }
        });
        return (inView);
    }
}