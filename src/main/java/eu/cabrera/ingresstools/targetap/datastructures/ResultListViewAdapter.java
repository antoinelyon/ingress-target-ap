package eu.cabrera.ingresstools.targetap.datastructures;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import eu.cabrera.ingresstools.targetap.R;

/**
 * Permet de mapper les résultats affichés avec la structure de résultats
 *
 * Created by antoine on 18/04/2014.
 */
public class ResultListViewAdapter extends ArrayAdapter {

    private final Context mContext;
    private final List<ActionResult> actionResultList;
    private final LayoutInflater inflater;

    public ResultListViewAdapter(Context context, int resource,
                                 int textViewResourceId, List <ActionResult> actionResultList) {
        super(context, resource, textViewResourceId, actionResultList);
        this.mContext = context;
        this.actionResultList = actionResultList;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return actionResultList.size();
    }
    public ActionResult getItem(int position) {
        return this.actionResultList.get(position);
    }

    public long getItemId(int position) {
        return  position;
    }

    public static class ViewHolder{
        TextView name;
        TextView nb;
    }

    @Override
    public View getView(int pos, View inView, ViewGroup parent) {
        ViewHolder holder;
        // Si la ligne n'éxiste pas > On créée la ligne
        // If row doesn't exist > we make it
        if (inView == null)
        {
            inView = this.inflater.inflate(R.layout.myresult,parent,false);
            holder = new ViewHolder();
            // On affecte les views
            // We get views
            holder.name = (TextView)  inView.findViewById(R.id.name);
            holder.nb = (TextView) inView.findViewById(R.id.nb);
            inView.setTag(holder);

            String fontPathR = "Coda-Regular.ttf";
            Typeface tfCodaR = Typeface.createFromAsset(mContext.getAssets(), fontPathR);
            holder.name.setTypeface(tfCodaR);
            holder.nb.setTypeface(tfCodaR);
        }
        // Sinon on récupère la ligne qui est en mémoire
        // Else we get row which is in memory
        else
            holder = (ViewHolder) inView.getTag();

        // On récupère l'objet courant
        // We get the current object
        ActionResult currentActionResult = this.actionResultList.get(pos);

        // On met à jour nos views
        // We update views

        String fontPathR = "Coda-Regular.ttf";
        Typeface tfCodaR = Typeface.createFromAsset(mContext.getAssets(), fontPathR);
        holder.name.setTypeface(tfCodaR);
        holder.nb.setTypeface(tfCodaR);
        holder.name.setText(currentActionResult.getActionName());
        holder.nb.setText(Integer.toString(currentActionResult.getNb()) + "x");

        return (inView);
    }


}
