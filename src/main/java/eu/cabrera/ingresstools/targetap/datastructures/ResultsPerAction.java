package eu.cabrera.ingresstools.targetap.datastructures;

import java.util.ArrayList;

/**
 *
 * Created by antoine on 12/05/2014.
 */
final class ResultsPerAction implements Comparable<ResultsPerAction> {
    private final int actions ;
    private int number ;
    private final  ArrayList <int[]> results ;

    public ResultsPerAction(int actions, int [] indexes){
        this.actions = actions ;
        this.number = 1 ;
        this.results = new ArrayList<int[]>() ;

        int[] res = new int[indexes.length] ;

        System.arraycopy (indexes,0,res,0,indexes.length);
        results.add(res);
    }

    public void setNumber(int number){
        this.number = number ;
    }

    public void addResults(int[] indexes){
        int[] res = new int[indexes.length] ;

        System.arraycopy (indexes,0,res,0,indexes.length);
        results.add(res);
    }

    public int getActions(){
        return this.actions ;
    }

    public int getNumber(){
        return this.number ;
    }

    public int[] getResults (int index){
        return results.get(index);
    }

    @Override
    public int compareTo( ResultsPerAction resultsPerAction){

        int compareQuantity = resultsPerAction.getActions();

        //ascending order
        return this.actions - compareQuantity;

        //descending order
        //return compareQuantity - this.quantity;
    }


}
