package eu.cabrera.ingresstools.targetap.datastructures;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * Created by antoine on 12/05/2014.
 */
public class ResultsPerActionList {
    private final  ArrayList<ResultsPerAction> liste;

    public int nbTotal ;

    public ResultsPerActionList(){
        liste = new ArrayList<ResultsPerAction>();
        nbTotal = 0 ;
    }

    public void update (int actions, int[] indexes){

        sort();

        boolean found = false ;

        for(ResultsPerAction resultPerAction : liste)

        if (resultPerAction.getActions() == actions){
            found = true ;
            resultPerAction.setNumber(resultPerAction.getNumber()+1);
            int [] maTable ;
            maTable = new int[indexes.length];
            System.arraycopy (indexes,0,maTable,0,indexes.length);
            resultPerAction.addResults(maTable);
            break ;
        }

        if (!found){
            int [] maTable ;
            maTable = new int[indexes.length];
            System.arraycopy (indexes,0,maTable,0,indexes.length);

            ResultsPerAction resultsPerAction = new ResultsPerAction(actions, maTable);
            liste.add(resultsPerAction);
        }

        nbTotal++ ;
        //Log.d("rpa", "update nbtotal =" + nbTotal) ;

    }

    private  void sort(){
        Collections.sort(liste);
    }


    public int getTotal() {
        return nbTotal ;
    }

    public int getNb (int indexX){

        return liste.get(indexX).getNumber() ;
    }

    public int getActions (int indexX){
        return liste.get(indexX).getActions() ;
    }

    public int[] getResults (int indexX, int indexY){

        return liste.get(indexX).getResults(indexY) ;
    }

    public IndexPair indexToPair (int index){
        //Log.d("rpa", "indextopair[" +index+"]");
        //Log.d("rpa", "liste size =" + liste.size());

        int cumul = 0 ;

        for (int i = 0; i<liste.size();i++){
            //Log.d("rpa","getnumber[" + i + "]=" + liste.get(i).getNumber());
            if (index>=cumul && index< (cumul+liste.get(i).getNumber()))
                return new IndexPair(i, index-cumul);
            cumul += liste.get(i).getNumber();
        }
        return new IndexPair (0,0);
    }

}
