package eu.cabrera.mathutils;

/**
 *
 * Created by antoine on 01/05/2014.
 */
public class MathUtil {

    /**
     * Calcul du PPCM de deux nombres
     */
    public static int getPpcm (int n1, int n2) {
        int p, r, ppcm;

        p = n1*n2;
        r   = n1%n2;
        while(r != 0){
            n1 = n2;
            n2 = r;
            r = n1%n2;
        }
        ppcm = p/n2;

        return ppcm;
    }
}
